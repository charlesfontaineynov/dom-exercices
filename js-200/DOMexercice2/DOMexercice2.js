/*
    Recréer l’exercice 1 sans balise HTML :
    Toute la conception du DOM doit être réalisée en JavaScript, n'hésitez pas à le faire de manière propre et pratique en utilisant des tableaux et des boucles.
*/

document.addEventListener('DOMContentLoaded', () => {
    // -- your code here, after the DOM has been fully loaded

    /* Global variables */
    
    const numberOfItems = 5;
    const numberOfTargets = 2;

    /* Create main div element */

    const mainDiv = document.createElement('div');
          mainDiv.id = "content";

    /* Create menu elements */

    const menu = document.createElement('div');
          menu.setAttribute('class', 'menu dark-grey');


    /* Creates menu items elements (tests with setAttribute and createTextNode methodes) */

    const itemTextNodes = ['cible 1', 'cible 2', 'cible 2', 'cible 1', 'cible 2'];

    for(let i = 0; i < numberOfItems; i++) {
      const item = document.createElement('div'),
            itemH2 = document.createElement('h2'),
            itemSpan = document.createElement('span'),
            index = document.createTextNode(i + 1),
            textNode = document.createTextNode(itemTextNodes[i]);

      item.setAttribute('class', 'item light-grey');

      itemSpan.appendChild(textNode);
      itemH2.appendChild(index);
      item.appendChild(itemH2);
      item.appendChild(itemSpan)

      menu.appendChild(item);
    }

    /* Create targets elements (tests with classList object to add classes and innerHTML property to insert html and texts) */

    const targets = document.createElement('div');
          targets.setAttribute('class', 'targets grey');

    for(let i = 0; i < numberOfTargets; i++) {
      const target = document.createElement('div'),
            targetH3 = document.createElement('h3'),
            targetSpan = document.createElement('span');

      target.classList.add('target');
      targetH3.innerHTML = `cible ${i + 1}`;
      targetSpan.classList.add('compteur');
      targetSpan.innerHTML = 0;

      target.appendChild(targetH3);
      target.appendChild(targetSpan);

      targets.appendChild(target)
    }

    targets.firstElementChild.classList.add('pink');
    targets.lastElementChild.classList.add('red');

    /* Append menu and target elements */

    mainDiv.appendChild(menu);
    mainDiv.appendChild(targets);

    /* Append main div element */

    document.body.appendChild(mainDiv);
})