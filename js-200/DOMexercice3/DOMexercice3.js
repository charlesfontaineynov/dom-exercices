/*
      Avec les événements et les manipulations des éléments, l'objectif est de réaliser un déplacement de DIV en drag & drop
      • récupérer les éléments de classe draggableBox

      Pour chacun d'entre eux mettre 2 écouteurs
      • un mousedown qui va initier le déplacement
      • un mouseup qui va stopper le déplacement

      Globalement, mettre un écouteur sur mousemouve qui va permettre le suivi du drag & drop
*/

const DRAGGABLE_ELEMENTS_WIDTH = 100
const DRAGGABLE_ELEMENTS_HEIGHT = 100

let currentContentWidth = null
let currentContentHeight = null

document.addEventListener('DOMContentLoaded', () => {
   onResize() // to initialize currentContentWidth / currentContentHeight
   renderDraggableElements()
   attachDragEvents()
})

function attachDragEvents() {
   //-- Exercice principal : Implémentez le drag and drop
   //-- Exercice bonus 1 : la dernière box relachée doit être au dessus des autres
   //-- Exercice bonus 2 : lorsque deux box sont en contact, elles doivent être teintes en rouge

   let draggableBoxes = document.querySelectorAll('.draggableBox');

   for(let i = 0; i < draggableBoxes.length; i++) {

      let draggableBox = draggableBoxes[i];

      draggableBox.addEventListener('mousedown', function(event) {
         this.style.zIndex = 1000;
         this.style.position = 'absolute';

         let posBoxX = event.clientX - this.getBoundingClientRect().left;
         let posBoxY = event.clientY - this.getBoundingClientRect().top;

         let self = this;

         const moveElementFrom = function(pageX, pageY) {
            self.style.left = pageX - posBoxX + 'px';
            self.style.top = pageY - posBoxY+ 'px';
         }

         const onMouseMove = function(event) {
            moveElementFrom(event.pageX, event.pageY);
         }

         document.addEventListener('mousemove', onMouseMove, false);

         document.addEventListener('mouseup', function(){
            document.removeEventListener('mousemove', onMouseMove, false);
         }, false);

         moveElementFrom(event.pageX, event.pageY);

         this.addEventListener('dragstart', function() {
            return false;
         }, false);

      }, false);
   }
}

function renderDraggableElements() {
   const contentElement = document.getElementById('content')
   const maxLeft = currentContentWidth - DRAGGABLE_ELEMENTS_WIDTH
   const maxTop = currentContentHeight - DRAGGABLE_ELEMENTS_HEIGHT

   for (let i = 0; i <= 10; i++) {
      const divElement = document.createElement('div')
      divElement.className = 'draggableBox'
      divElement.appendChild(document.createTextNode(`Box nº${i}`))
      divElement.style.left = Math.floor(Math.random() * maxLeft) + 'px'
      divElement.style.top = Math.floor(Math.random() * maxTop) + 'px'
      contentElement.appendChild(divElement)
   }
}

window.addEventListener('optimizedResize', onResize)

function onResize() {
   const contentElement = document.getElementById('content')

   //-- Exercice Bonus 3: implémenter ici le repositionnement des box lorsque la fenêtre change de taille, les box doivent proportionnellement se retrouver à la même place

   currentContentWidth = contentElement.offsetWidth
   currentContentHeight = contentElement.offsetHeight
}

// See https://developer.mozilla.org/en-US/docs/Web/Events/resize
// Prevent resize event to be fired way too often, this means neither lags nor freezes
{
   function throttle(type, name, obj = window) {
      let running = false
      const event = new CustomEvent(name)
      obj.addEventListener(type, () => {
         if (running) return
         running = true
         requestAnimationFrame(() => {
            obj.dispatchEvent(event)
            running = false
         })
      })
   }

   /* init - you can init any event */
   throttle('resize', 'optimizedResize');
}