/*
    Parcourir tous les éléments dont la classe est "item"

    • Dès que le texte à l'intérieur correspond à "cible 1"
        • Retirer la classe "light-grey" de l'item
        • Ajouter la classe "pink" à cet item
        • Incrémenter la valeur du compteur de l'élément HTML correspondant à la classe "target" de la cible 1

    • Dès que le texte à l'intérieur correspond à "cible 2"
        • Retirer la classe "light-grey" de l'item
        • Ajouter la classe "red" à cet item
        • Incrémenter la valeur du compteur de l'élément HTML correspondant à la classe "target" de la cible 2
*/

document.addEventListener('DOMContentLoaded', () => {
    // -- your code here, after the DOM has been fully loaded

    const setTarget = function(target) {

       const counterTarget = document.querySelectorAll(`.targets ${target} .compteur`);

       // In case multiple target blocks of type 1 and 2 provided
       // In this example, only 2 blocks type target 1 and target 2 inside HTML code
       for(let i = 0; i < counterTarget.length; i++) {
           let counterTargetNum = window.parseInt(counterTarget[i].innerHTML, 10);

           ++counterTargetNum;
           counterTarget[i].innerHTML = counterTargetNum;
       }
    }

    let items = document.querySelectorAll('.item');


    for(let i = 0; i < items.length; i++) {
        let item = items[i];

        if(item.querySelector('span').innerHTML === 'cible 1') {

            item.classList.remove('light-grey');
            item.classList.add('pink');

            setTarget('.pink');
        }

        if(item.querySelector('span').innerHTML === 'cible 2') {

            item.classList.remove('light-grey');
            item.classList.add('red');

            setTarget('.red');
        }
    }
})